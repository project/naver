<?php

/**
 * @file
 * Configuration form implementation.
 */

/**
 * Page callback.
 */
function naver_settings_form($form, &$form_state) {
  if (!naver_library_detect()) {
    form_set_error('form', t('Naver library not found. Please, check README.txt for instructions.'));
    return array();
  }

  $settings = variable_get('naver_settings', array());

  $form['naver_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Naver'),
    '#default_value' => variable_get('naver_enabled', 0)
  );

  $form['naver_settings'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => 'Naver settings',
    '#states' => array(
      'visible' => array(
        ':input[name="naver_enabled"]' => array('checked' => TRUE),
      ),
    )
  );

  $form['naver_settings']['element'] = array(
    '#type' => 'textfield',
    '#title' => t('Target element'),
    '#default_value' => isset($settings['element']) ? $settings['element'] : '',
    '#description' => t('Target element to apply Naver.'),
    '#states' => array(
      'required' => array(
        ':input[name="naver_enabled"]' => array('checked' => TRUE),
      ),
    )
  );

  $form['naver_settings']['breakpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Breakpoint'),
    '#default_value' => isset($settings['breakpoint']) ? $settings['breakpoint'] : '',
    '#description' => t('Width at which to auto-disable Naver. Example: 768px.'),
  );

  $form['naver_settings']['class'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom class'),
    '#default_value' => isset($settings['class']) ? $settings['class'] : '',
    '#description' => t('Class applied to instance.'),
  );

  $form['naver_settings']['label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display handle with a label'),
    '#default_value' => isset($settings['label']) ? $settings['label'] : 1
  );

  $form['naver_settings']['label_closed'] = array(
    '#type' => 'textfield',
    '#title' => t('Closed state text'),
    '#default_value' => isset($settings['label_closed']) ? $settings['label_closed'] : '',
    '#states' => array(
      'visible' => array(
        ':input[name="naver_settings[label]"]' => array('checked' => TRUE),
      ),
    )
  );

  $form['naver_settings']['label_open'] = array(
    '#type' => 'textfield',
    '#title' => t('Open state text'),
    '#default_value' => isset($settings['label_open']) ? $settings['label_open'] : '',
    '#states' => array(
      'visible' => array(
        ':input[name="naver_settings[label]"]' => array('checked' => TRUE),
      ),
    )
  );

  return system_settings_form($form);
}

/**
 * Form validation callback.
 */
function naver_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if ($values['naver_enabled'] && empty($values['naver_settings']['element'])) {
    form_set_error('naver_settings][element', t('Please, provide element to apply Naver.'));
  }
}

