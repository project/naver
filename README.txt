
NAVER
-----

Naver is a simple and robust jQuery plugin for responsive navigation.
Configuration is very simple. Just provide an element to apply the plugin
and specify your breakpoint. Please, visit official page for more information
and demo (http://formstone.it/components/Naver/demo/index.html).

INSTALLATION
------------

1. Install the module as usual. Here assumed that the module will be installed
within sites/all/modules/ directory.
2. Download and extract library's files into sites/all/libraries/naver
directory. Also there is more faster way to download library. Simply execute
next Drush command within the document root:
drush make sites/all/modules/naver/naver.make --no-core .
3. Visit admin/config/user-interface/naver page. Set your breakpoint and target element. Test it.

