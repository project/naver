(function ($) {
  Drupal.behaviors.Naver = {
    attach: function (context, settings) {
      var config = settings.naver;
      $(config.element, context).naver({
        customClass: config.customClass,
        label: config.label,
        labels: {
          closed: config.labelClosed,
          open: config.labelOpen
        },
        maxWidth: config.maxWidth
      });
    }
  };
})(jQuery);

